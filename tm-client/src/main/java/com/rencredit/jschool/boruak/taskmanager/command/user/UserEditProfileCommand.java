package com.rencredit.jschool.boruak.taskmanager.command.user;

import com.rencredit.jschool.boruak.taskmanager.command.AbstractCommand;
import com.rencredit.jschool.boruak.taskmanager.endpoint.*;
import com.rencredit.jschool.boruak.taskmanager.enumerated.Role;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.EmptyUserException;
import com.rencredit.jschool.boruak.taskmanager.util.TerminalUtil;
import com.rencredit.jschool.boruak.taskmanager.util.ViewUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UserEditProfileCommand extends AbstractCommand {

    @Autowired
    private AuthEndpoint authEndpoint;

    @Autowired
    private UserEndpoint userEndpoint;

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "edit-profile";
    }

    @NotNull
    @Override
    public String description() {
        return "Edit profile.";
    }

    @Override
    public void execute() throws EmptyLastNameException_Exception, EmptyEmailException_Exception, DeniedAccessException_Exception, EmptyMiddleNameException_Exception, EmptyIdException_Exception, EmptyFirstNameException_Exception, EmptyUserException_Exception, EmptyUserException {
        System.out.println("Enter email");
        @NotNull final String email = TerminalUtil.nextLine();
        System.out.println("Enter first name");
        @NotNull final String firstName = TerminalUtil.nextLine();
        System.out.println("Enter last name");
        @NotNull final String lastName = TerminalUtil.nextLine();
        System.out.println("Enter middle name");
        @NotNull final String middleName = TerminalUtil.nextLine();

        @NotNull final SessionDTO session = systemObjectService.getSession();
        @Nullable final String userId = authEndpoint.getUserId(session);
        @Nullable final UserDTO user = userEndpoint.editUserProfileByIdFirstNameLastNameMiddleName(session, userId, email, firstName, lastName, middleName);
        ViewUtil.showUser(user);

    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN, Role.USER};
    }

}
