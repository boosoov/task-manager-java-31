package com.rencredit.jschool.boruak.taskmanager.command.task;

import com.rencredit.jschool.boruak.taskmanager.command.AbstractCommand;
import com.rencredit.jschool.boruak.taskmanager.endpoint.*;
import com.rencredit.jschool.boruak.taskmanager.enumerated.Role;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.EmptyTaskException;
import com.rencredit.jschool.boruak.taskmanager.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class TaskShowByIndexCommand extends AbstractCommand {

    @Autowired
    private TaskEndpoint taskEndpoint;

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "task-view-by-index";
    }

    @NotNull
    @Override
    public String description() {
        return "Show task by index.";
    }

    @Override
    public void execute() throws DeniedAccessException_Exception, EmptyUserIdException_Exception, IncorrectIndexException_Exception, EmptyTaskException {
        System.out.println("[SHOW TASK]");
        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;

        @NotNull final SessionDTO session = systemObjectService.getSession();
        @Nullable final TaskDTO task = taskEndpoint.findTaskByIndex(session, index);
        TerminalUtil.showTask(task);
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN, Role.USER};
    }

}
