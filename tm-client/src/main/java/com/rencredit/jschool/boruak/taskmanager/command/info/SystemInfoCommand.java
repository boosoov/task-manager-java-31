package com.rencredit.jschool.boruak.taskmanager.command.info;

import com.rencredit.jschool.boruak.taskmanager.command.AbstractCommand;
import com.rencredit.jschool.boruak.taskmanager.service.InfoService;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public final class SystemInfoCommand extends AbstractCommand {

    @Autowired
    private InfoService infoService;

    @NotNull
    @Override
    public String arg() {
        return "-i";
    }

    @NotNull
    @Override
    public String name() {
        return "info";
    }

    @NotNull
    @Override
    public String description() {
        return "Show computer info.";
    }

    @Override
    public void execute() {
        System.out.println("[SYSTEM INFO]");
        System.out.println(infoService.getSystemInfo());
    }

}
