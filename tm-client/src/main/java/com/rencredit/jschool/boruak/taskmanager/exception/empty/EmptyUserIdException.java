package com.rencredit.jschool.boruak.taskmanager.exception.empty;

import com.rencredit.jschool.boruak.taskmanager.exception.AbstractClientException;

public class EmptyUserIdException extends AbstractClientException {

    public EmptyUserIdException() {
        super("Error! User id is empty...");
    }

}
