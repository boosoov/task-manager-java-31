package com.rencredit.jschool.boruak.taskmanager.boorstrap;

import com.rencredit.jschool.boruak.taskmanager.api.service.ICommandService;
import com.rencredit.jschool.boruak.taskmanager.command.AbstractCommand;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.EmptyCommandException;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.EmptyNameException;
import com.rencredit.jschool.boruak.taskmanager.exception.unknown.UnknownCommandException;
import com.rencredit.jschool.boruak.taskmanager.util.TerminalCommandUtil;
import com.rencredit.jschool.boruak.taskmanager.util.TerminalUtil;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.LinkedHashMap;
import java.util.Map;

@Component
public class Bootstrap {

    @Autowired
    private ICommandService commandService;

    public Bootstrap() {
    }

    public void run(@Nullable final String[] args) {
        @NotNull String[] commandsFromUser = null;
        System.out.println("** WELCOME TO TASK MANAGER ** \n");
        parseArgs(args);
        while (true) {
            try {
                commandsFromUser = TerminalUtil.nextLine().split("\\s+");
                parseArgs(commandsFromUser);
            } catch (Exception e) {
                System.err.println(e.getMessage());
            }
        }
    }
    @Autowired
    private void registryCommand(@NotNull final AbstractCommand[] abstractCommands) throws EmptyCommandException, EmptyNameException {
        if (abstractCommands == null) return;
        for (AbstractCommand command : abstractCommands) {
            commandService.putCommand(command.name(), command);
        }
    }

    public void parseArgs(@Nullable final String[] args) {
        if (args == null || args.length == 0) return;
        for (@Nullable final String arg : args) {
            processArg(TerminalCommandUtil.convertArgumentToCommand(arg));
            System.out.println();
        }
    }

    @SneakyThrows
    private void processArg(@Nullable final String cmd) {
        if (cmd == null || cmd.isEmpty()) return;
        @Nullable final AbstractCommand command = commandService.getTerminalCommands().get(cmd);
        if (command == null) throw new UnknownCommandException(cmd);
        command.execute();
    }

}
