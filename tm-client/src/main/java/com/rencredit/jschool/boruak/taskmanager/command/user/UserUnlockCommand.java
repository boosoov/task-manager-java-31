package com.rencredit.jschool.boruak.taskmanager.command.user;

import com.rencredit.jschool.boruak.taskmanager.command.AbstractCommand;
import com.rencredit.jschool.boruak.taskmanager.endpoint.*;
import com.rencredit.jschool.boruak.taskmanager.enumerated.Role;
import com.rencredit.jschool.boruak.taskmanager.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UserUnlockCommand extends AbstractCommand {

    @Autowired
    private AdminUserEndpoint adminUserEndpoint;

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "unlock-user";
    }

    @NotNull
    @Override
    public String description() {
        return "Unlock user";
    }

    @Override
    public void execute() throws NotExistUserException_Exception, EmptyRoleException_Exception, DeniedAccessException_Exception, EmptySessionException_Exception, EmptyIdException_Exception, EmptyUserIdException_Exception, EmptyLoginException_Exception, EmptyUserException_Exception {
        System.out.println("[UNLOCK USER]");
        System.out.println("ENTER LOGIN");
        @NotNull final String login = TerminalUtil.nextLine();

        @Nullable final SessionDTO session = systemObjectService.getSession();
        adminUserEndpoint.unlockUserByLogin(session, login);
        System.out.println("OK");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}
