package com.rencredit.jschool.boruak.taskmanager.exception.notexist;

import com.rencredit.jschool.boruak.taskmanager.exception.AbstractClientException;

public class NotExistUserException extends AbstractClientException {

    public NotExistUserException() {
        super("Error! User is not exist...");
    }

}
