package com.rencredit.jschool.boruak.taskmanager.command.project;

import com.rencredit.jschool.boruak.taskmanager.command.AbstractCommand;
import com.rencredit.jschool.boruak.taskmanager.endpoint.DeniedAccessException_Exception;
import com.rencredit.jschool.boruak.taskmanager.endpoint.EmptyUserIdException_Exception;
import com.rencredit.jschool.boruak.taskmanager.endpoint.ProjectEndpoint;
import com.rencredit.jschool.boruak.taskmanager.endpoint.SessionDTO;
import com.rencredit.jschool.boruak.taskmanager.enumerated.Role;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ProjectListClearCommand extends AbstractCommand {

    @Autowired
    private ProjectEndpoint projectEndpoint;

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "project-clear";
    }

    @NotNull
    @Override
    public String description() {
        return "Remove all projects.";
    }

    @Override
    public void execute() throws EmptyUserIdException_Exception, DeniedAccessException_Exception {
        System.out.println("[CLEAR PROJECTS]");

        @NotNull final SessionDTO session = systemObjectService.getSession();
        projectEndpoint.clearAllUserProjects(session);
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN, Role.USER};
    }

}
