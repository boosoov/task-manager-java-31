package com.rencredit.jschool.boruak.taskmanager.endpoint;

import com.rencredit.jschool.boruak.taskmanager.api.endpoint.ITaskEndpoint;
import com.rencredit.jschool.boruak.taskmanager.api.service.ITaskService;
import com.rencredit.jschool.boruak.taskmanager.dto.SessionDTO;
import com.rencredit.jschool.boruak.taskmanager.dto.TaskDTO;
import com.rencredit.jschool.boruak.taskmanager.exception.busy.BusyLoginException;
import com.rencredit.jschool.boruak.taskmanager.exception.denied.DeniedAccessException;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.*;
import com.rencredit.jschool.boruak.taskmanager.exception.incorrect.IncorrectIndexException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
@Controller
public class TaskEndpoint extends AbstractEndpoint implements ITaskEndpoint {

    @Nullable
    @Autowired
    private ITaskService taskService;

    public TaskEndpoint() {
    }

    @Override
    @WebMethod
    public void createTaskName(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO session,
            @Nullable @WebParam(name = "name", partName = "name") final String name
    ) throws EmptyNameException, EmptyUserIdException, DeniedAccessException {
        sessionService.validate(session);
        taskService.create(session.getUserId(), name);
    }

    @Override
    @WebMethod
    public void createTaskNameDescription(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO session,
            @Nullable @WebParam(name = "name", partName = "name") final String name,
            @Nullable @WebParam(name = "description", partName = "description") final String description
    ) throws EmptyNameException, EmptyDescriptionException, EmptyUserIdException, DeniedAccessException {
        sessionService.validate(session);
        taskService.create(session.getUserId(), name, description);
    }

    @NotNull
    @Override
    @WebMethod
    public List<TaskDTO> findAllTaskByUserId(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO session
    ) throws EmptyUserIdException, DeniedAccessException {
        sessionService.validate(session);
        return taskService.findAllByUserId(session.getUserId());
    }

    @Nullable
    @Override
    @WebMethod
    public TaskDTO findTaskById(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO session,
            @Nullable @WebParam(name = "id", partName = "id") final String id
    ) throws EmptyIdException, EmptyUserIdException, DeniedAccessException {
        sessionService.validate(session);
        return taskService.findOneById(session.getUserId(), id);
    }

    @Nullable
    @Override
    @WebMethod
    public TaskDTO findTaskByIndex(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO session,
            @Nullable @WebParam(name = "index", partName = "index") final Integer index
    ) throws EmptyUserIdException, IncorrectIndexException, DeniedAccessException {
        sessionService.validate(session);
        return taskService.findOneByIndex(session.getUserId(), index);
    }

    @Nullable
    @Override
    @WebMethod
    public TaskDTO findTaskByName(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO session,
            @Nullable @WebParam(name = "name", partName = "name") final String name
    ) throws EmptyUserIdException, EmptyNameException, DeniedAccessException {
        sessionService.validate(session);
        return taskService.findOneByName(session.getUserId(), name);
    }

    @NotNull
    @Override
    @WebMethod
    public TaskDTO updateTaskById(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO session,
            @Nullable @WebParam(name = "id", partName = "id") final String id,
            @Nullable @WebParam(name = "name", partName = "name") final String name,
            @Nullable @WebParam(name = "description", partName = "description") final String description
    ) throws EmptyTaskException, EmptyIdException, EmptyUserIdException, EmptyNameException, DeniedAccessException, EmptyDescriptionException {
        sessionService.validate(session);
        return taskService.updateTaskById(session.getUserId(), id, name, description);
    }

    @NotNull
    @Override
    @WebMethod
    public TaskDTO updateTaskByIndex(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO session,
            @Nullable @WebParam(name = "index", partName = "index") final Integer index,
            @Nullable @WebParam(name = "name", partName = "name") final String name,
            @Nullable @WebParam(name = "description", partName = "description") final String description
    ) throws IncorrectIndexException, EmptyTaskException, EmptyNameException, EmptyUserIdException, DeniedAccessException, EmptyDescriptionException {
        sessionService.validate(session);
        return taskService.updateTaskByIndex(session.getUserId(), index, name, description);
    }

    @Nullable
    @Override
    @WebMethod
    public void removeTaskById(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO session,
            @Nullable @WebParam(name = "id", partName = "id") final String id
    ) throws EmptyIdException, EmptyUserIdException, DeniedAccessException {
        sessionService.validate(session);
        taskService.removeOneById(session.getUserId(), id);
    }

    @Nullable
    @Override
    @WebMethod
    public void removeTaskByIndex(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO session,
            @Nullable @WebParam(name = "index", partName = "index") final Integer index
    ) throws EmptyUserIdException, IncorrectIndexException, DeniedAccessException {
        sessionService.validate(session);
        taskService.removeOneByIndex(session.getUserId(), index);
    }

    @Nullable
    @Override
    @WebMethod
    public void removeTaskByName(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO session,
            @Nullable @WebParam(name = "name", partName = "name") final String name
    ) throws EmptyNameException, EmptyUserIdException, DeniedAccessException {
        sessionService.validate(session);
        taskService.removeOneByName(session.getUserId(), name);
    }

    @Override
    @WebMethod
    public void removeAllUserTasks(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO session
    ) throws EmptyUserIdException, DeniedAccessException {
        sessionService.validate(session);
        taskService.clearByUserId(session.getUserId());
    }

    @Override
    @WebMethod
    public void clearAllTask(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO session
    ) throws DeniedAccessException, EmptyUserException, EmptyHashLineException, BusyLoginException, EmptyPasswordException, EmptyLoginException, EmptyRoleException {
        sessionService.validate(session);
        taskService.clearAll();
    }

    @NotNull
    @Override
    @WebMethod
    public List<TaskDTO> getListTask(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO session
    ) throws DeniedAccessException {
        sessionService.validate(session);
        return taskService.getList();
    }

}
