package com.rencredit.jschool.boruak.taskmanager.api.repository;

import com.rencredit.jschool.boruak.taskmanager.dto.UserDTO;
import com.rencredit.jschool.boruak.taskmanager.entity.User;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface IUserRepository extends IRepository<UserDTO> {

    @NotNull
    List<UserDTO> getListDTO();

    @NotNull
    List<User> getListEntity();

    @Nullable
    UserDTO findByIdDTO(@NotNull String id);

    @Nullable
    User findByIdEntity(@NotNull final String id);

    @Nullable
    UserDTO findByLoginDTO(@NotNull String name);

    @Nullable
    User findByLoginEntity(@NotNull final String login);

    void removeById(@NotNull String id);

    void removeByLogin(@NotNull String name);

    void removeByUser(@NotNull UserDTO user);

    void clearAll();

    void addUser(@NotNull final User user);

}
