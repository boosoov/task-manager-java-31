package com.rencredit.jschool.boruak.taskmanager.endpoint;

import com.rencredit.jschool.boruak.taskmanager.api.service.IAuthService;
import com.rencredit.jschool.boruak.taskmanager.api.service.ISessionService;
import com.rencredit.jschool.boruak.taskmanager.enumerated.Role;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import javax.jws.WebService;

@Controller
@WebService
public class AbstractEndpoint {

    @Nullable
    @Autowired
    ISessionService sessionService;

    @Nullable
    @Autowired
    IAuthService authService;

    @Nullable
    public Role[] roles() {
        return null;
    }

    public AbstractEndpoint() {
    }

}
