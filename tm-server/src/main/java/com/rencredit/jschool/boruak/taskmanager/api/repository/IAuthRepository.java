package com.rencredit.jschool.boruak.taskmanager.api.repository;

import com.rencredit.jschool.boruak.taskmanager.dto.UserDTO;
import org.jetbrains.annotations.NotNull;

public interface IAuthRepository {

    void logIn(@NotNull UserDTO user);

    void logOut();

}
