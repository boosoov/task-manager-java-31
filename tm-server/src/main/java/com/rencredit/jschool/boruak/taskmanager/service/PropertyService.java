package com.rencredit.jschool.boruak.taskmanager.service;

import com.rencredit.jschool.boruak.taskmanager.api.service.IDataBasePropertyService;
import com.rencredit.jschool.boruak.taskmanager.api.service.IPropertyService;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

@Service
public class PropertyService implements IPropertyService, IDataBasePropertyService {

    @NotNull
    private final static String NAME = "/application.properties";

    @NotNull
    private final Properties properties = new Properties();

    public PropertyService() {
        init();
    }

    @Override
    public boolean init() {
        @NotNull final InputStream inputStream = PropertyService.class.getResourceAsStream(NAME);
        try {
            properties.load(inputStream);
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    @NotNull
    @Override
    public String getServerHost() {
        @NotNull final String propertyHost = properties.getProperty("server.host");
        @NotNull final String envHost = System.getenv("tm_server_host");
        if (envHost != null) return envHost;
        return propertyHost;
    }

    @NotNull
    @Override
    public Integer getServerPort() {
        @NotNull final String propertyPort = properties.getProperty("server.port");
        @NotNull final String envPort = System.getenv("tm_server_port");
        @NotNull String value = propertyPort;
        if (envPort != null) value = envPort;
        return Integer.parseInt(value);
    }

    @NotNull
    @Override
    public String getSessionSalt() {
        return properties.getProperty("session.salt");
    }

    @NotNull
    @Override
    public Integer getSessionCycle() {
        return Integer.parseInt(properties.getProperty("session.cycle"));
    }

    @NotNull
    @Override
    public String getJdbcDriver() {
        @NotNull final String jdbcDriver = System.getenv("jdbcDriver");
        if(jdbcDriver != null) return jdbcDriver;
        return properties.getProperty("jdbcDriver");
    }

    @NotNull
    @Override
    public String getJdbcUrl() {
        @NotNull final String hostDB = System.getenv("db_host");
        if(hostDB != null) return hostDB;
        return properties.getProperty("db.host");
    }

    @NotNull
    @Override
    public String getJdbcUsername() {
        @NotNull final String loginDB = System.getenv("db_login");
        if(loginDB != null) return loginDB;
        return properties.getProperty("db.login");
    }

    @NotNull
    @Override
    public String getJdbcPassword() {
        @NotNull final String passwordDB = System.getenv("db_password");
        if(passwordDB != null) return passwordDB;
        return properties.getProperty("db.password");
    }

}
