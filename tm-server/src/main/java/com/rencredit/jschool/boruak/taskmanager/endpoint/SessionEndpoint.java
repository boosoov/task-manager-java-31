package com.rencredit.jschool.boruak.taskmanager.endpoint;

import com.rencredit.jschool.boruak.taskmanager.api.endpoint.ISessionEndpoint;
import com.rencredit.jschool.boruak.taskmanager.dto.SessionDTO;
import com.rencredit.jschool.boruak.taskmanager.dto.status.Fail;
import com.rencredit.jschool.boruak.taskmanager.dto.status.Result;
import com.rencredit.jschool.boruak.taskmanager.dto.status.Success;
import com.rencredit.jschool.boruak.taskmanager.exception.denied.DeniedAccessException;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.EmptyHashLineException;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.EmptyLoginException;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.EmptyPasswordException;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.EmptyUserException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Controller;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
@Controller
public class SessionEndpoint extends AbstractEndpoint implements ISessionEndpoint {

    public SessionEndpoint() {
    }

    @Nullable
    @Override
    @WebMethod
    public SessionDTO openSession (
            @Nullable @WebParam(name = "login", partName = "login") final String login,
            @Nullable @WebParam(name = "password", partName = "password") final String password
    ) throws EmptyUserException, EmptyHashLineException, EmptyLoginException, DeniedAccessException, EmptyPasswordException {
        return sessionService.open(login, password);
    }

    @NotNull
    @Override
    @WebMethod
    public Result closeSession(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO session
    ) throws DeniedAccessException {
        sessionService.validate(session);
        sessionService.close(session);
        return new Success();
    }

    @NotNull
    @Override
    @WebMethod
    public Result closeSessionAll(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO session
    ) throws DeniedAccessException {
        sessionService.validate(session);
        try {
            sessionService.closeAll(session);
            return new Success();
        } catch (final Exception e) {
            return new Fail(e);
        }
    }

}
